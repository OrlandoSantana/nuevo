package com.example.usuario.pruebamovil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.database.Cursor;

public class MainActivity extends Activity {

    EditText et_ciudad, et_nombre, et_capacidad, et_fecha;
    Button button_guardar, button_mostrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        button_guardar = (Button)findViewById(R.id.btnguardar);
        button_mostrar = (Button)findViewById(R.id.btnconsulta);
        et_ciudad = (EditText)findViewById(R.id.et_ciudad);
        et_capacidad = (EditText)findViewById(R.id.et_capacidad);
        et_nombre = (EditText)findViewById(R.id.et_nombre);
        et_fecha = (EditText)findViewById(R.id.et_fecha);


        final HelperDB helperbd = new HelperDB(getApplicationContext());


        button_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = helperbd.getWritableDatabase();
                ContentValues valores = new ContentValues();
                valores.put("ciudad",et_ciudad.getText().toString());
                valores.put("nombre", et_nombre.getText().toString());
                valores.put("capacidad", et_capacidad.getText().toString());
                valores.put("fecha", et_fecha.getText().toString());


                Long IdGuardado = db.insert("parametros", "ciudad",  valores);
                Toast.makeText(getApplicationContext(),
                        "Se guardo el dato: "+IdGuardado+ et_nombre.getText().toString(),
                        Toast.LENGTH_LONG).show();

            }
        });



        button_mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent (MainActivity.this,Resultado.class);
                startActivity(intent);




            }
        });

    }



}
